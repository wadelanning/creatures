'''

Wade Lanning 29 April 2019
This is where the different types of creatures and their stats are stored.
The stats are kept as dictionaries.

'''

### First define each creature type's stats
preyvariety = {
    "variety":"prey",
    "hpmax":100,
    "health":100,
    "sight":100,
    "reach":16,
    "speed":4,
    "damage":5,
	"age": 0
    }

predvariety = {
    "variety":"pred",
    "hpmax":500,
    "health":500,
    "sight":120,
    "reach":16,
    "speed":7,
    "damage":10,
	"age":0
    }

### Now make a master dictionary of all the stats
varietydict = {
    "prey":preyvariety,
    "pred":predvariety
    }
	
def assignvariety(varname): 
	return varietydict[varname].copy()