from freegames import vector

# Old commands (before classes)
def lookaround(creature,list):
	seenlist = []
	for item in list: 
		direction = item["position"] - creature["position"]
		if abs(direction) <= creature["sight"]: # can creature "see" item?
			seenlist.append([item["type"],direction])
	return seenlist
	
def simplehunt(creature, seenlist):
	nearest = vector(creature["sight"],creature["sight"])
	for item in seenlist: 
		if abs(item[1]) < abs(nearest) and item[0]=="prey": nearest = item[1]
	return nearest
	
def findandattack(creature, list):
	for i in range(0,len(list)): 
		direction = list[i]["position"] - creature["position"]
		if abs(direction) <= creature["reach"] and list[i]["type"]!="pred": 
			return [i,creature["damage"]] # return the damage
	return [0,0] # Do nothing if no target found
	
	
# Class-based commands
def pursuenearest(creature,sightlist,crelist):
	nearestid = sightlist[0][1]
	xdir = crelist[nearestid].x-creature.x
	ydir = crelist[nearestid].y-creature.y
	if xdir != 0 and ydir != 0 and creature.var != crelist[nearestid].var:	return xdir, ydir
	else: return creature.vx, creature.vy
	
def avoidnearest(creature,sightlist,crelist):
	nearestid = sightlist[0][1]
	xdir = crelist[nearestid].x-creature.x
	ydir = crelist[nearestid].y-creature.y
	if xdir != 0 and ydir != 0:	return -xdir, -ydir
	else: return creature.vx, creature.vy
	
def attacknearest(creature,sightlist,crelist):
	nearestid = sightlist[0][1]
	nearestdist = sightlist[0][0]
	if creature.stats["reach"] >= nearestdist: return nearestid, creature.stats["damage"]
	else: return nearestid, 0