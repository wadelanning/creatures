# Creatures
Creatures a simple Python game where two classes of computer players, predators and prey, compete to survive. 
Creatures is a work in progress intended to become a tool for training competing neural networks.
The current version just has creatures which chase one another based on simple geometric algorithms. The necessary scoring and decision-making APIs for neural network training have not been developed yet.

## Installation

1) Install python 3.6.3

2) Navigate to the directory containing the requirements.txt file and use pip to install dependencies
$ pip install -r requirements.txt


## Usage

$ python Creatures3.py

## Author
[Wade Lanning](https://wadelanning.com)

## License
[MIT](https://choosealicense.com/licenses/mit/)