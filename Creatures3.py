"""
CREATURES
A game where predator creatures hunt prey creatures
Wade Lanning 28 April 2019

GOAL: 
To create a framework where players folowing different rules (predators and prey) can make their own decisions
The objective will be to ultimately have different reinforcement learning algorithms competing with one another
For now, we just have some simple geometry-based decision making 

BUGS:
- Predator teleportation, bobbling, and running off the edge
- Predators reproducing, then pathing over one another till one starves (possibly fixed by new "avoid" line)
- Turn count (probably a scope problem) - this will be important to sort out before we can train a neural network

TO DO: 
- Clean up decision-making and implement "action queue" idea
- Turn script into a self-contained command which returns a "score"
- 2 scores: avg. pred life and avg. prey population 
"""

from random import randrange, randint
from turtle import *
from freegames import vector
from creaturecontrol import *
import math

screensize = (1200,900)
screenedge = 100

creatures = [] # List of creatures
turncount = 0 # For keeping track of how many turns the game has run

# Speed controls
basetimer = 10
basedist = 10



class LiveCreature: # Define the creature class
	def __init__(self, name, pos, variety): # Store creature characteristics here
		self.name = name
		self.var = variety
		self.x = pos[0]
		self.y = pos[1]
		self.vx = 0
		self.vy = 0
		self.stats = assignvariety(variety) # There are different kinds of creatures with different stats

	def move(self, xdir, ydir): # Move the creature
		if xdir != 0: self.x += self.stats["speed"] * xdir / math.hypot(xdir,ydir)
		if ydir != 0: self.y += self.stats["speed"] * ydir / math.hypot(xdir,ydir)
		
	def lookaround(self, list): # Take a survey to detect nearby creatures
		visiblelist = []
		for i in range(0,len(list)):
			distance = math.hypot(self.x-list[i].x,self.y-list[i].y)
			if 0 < distance <= self.stats["sight"]:
				visiblelist.append([distance,i])
		return sorted(visiblelist)

def inside(xy): # For checking to see whether or not something is in the field of play
    "Return True if xy within screen."
    return abs(xy.x) < screensize[0]/2+screenedge and abs(xy.y) < screensize[1]/2+screenedge

def draw(): # Represent creatures as color-coded dots
	clear()
	for creature in creatures:
		goto(creature.x, creature.y)
		if creature.var=="prey": dot(basedist*2, 'green')
		elif creature.var=="pred": dot(basedist*2, 'red')
		else: print("ERROR")
	update()
	


#### SETTING UP THE PREDATORS AND PREY
for i in range(0,20): # Add starting prey
		creatures.append( LiveCreature(1,[0,0],"prey") )
		creatures[-1].vx, creatures[-1].vy = randint(-10,10), randint(-10,10)
		
for i in range(0,1): # Add starting predators
		creatures.append( LiveCreature(1,[0,0],"pred") )
		creatures[-1].vx, creatures[-1].vy = randint(-10,10), randint(-10,10)	
	
#### GAME ENGINE	
def move(): # This recursive function repeats, incrementing the game by 1 "move" each time	

	# look: creatures look at surroundings
	# decide: each makes a decision about how to move based on surroundings
	for creature in creatures: 	
		surroundings = creature.lookaround(creatures) # identify nearby creatures
		# Prey avoid other creatures
		if len(surroundings) > 0 and creature.var=="prey": creature.vx, creature.vy = avoidnearest(creature,surroundings,creatures)
		
		# Predators chase nearby prey
		if len(surroundings) > 0 and creature.var=="pred": 
			creature.vx, creature.vy = avoidnearest(creature,surroundings,creatures) # Makes predators avoid each other 
			creature.vx, creature.vy = pursuenearest(creature,surroundings,creatures) # Unless they see prey, which they will chase
			target, damage = attacknearest(creature,surroundings,creatures) # Attacks will only work if creature is close enough
			creatures[target].stats["health"] -= damage # Update attacked prey health
			creature.stats["health"] += damage # Predator gains health from attacking
			if creature.stats["health"] >= creature.stats["hpmax"]: creature.stats["health"] = creature.stats["hpmax"] # Creature health cannot exceed maximum
			
			
	# action queue - placeholders, I ultimate want to resolve actual actions like attacking separately from decisions
	# resolve actions
	
	# move		
	for creature in creatures: 	
		# Creatures bounce off sides
		if abs(creature.x) >= screensize[0]/2: creature.vx = -creature.vx
		if abs(creature.y) >= screensize[1]/2: creature.vy = -creature.vy
		if abs(creature.x) >= screensize[0]/2+10: creature.x = creature.x/2
		if abs(creature.y) >= screensize[1]/2+10: creature.y = creature.y/2
		# Move the creature
		creature.move(creature.vx,creature.vy)

	# population
	dupe = creatures.copy() # Make a copy of the creature list to iterate through
	creatures.clear() # Make room to re-populate the list with creatures that survive

	predatorcensus = 0 # For counting the predator population
	for creature in dupe:
		if creature.stats["health"] > 0:  # creatures must have health to survive
			creature.stats["age"] += 1
			if creature.var == "pred": 
				creature.stats["health"] -= 1 # Predators slowly starve
				predatorcensus +=1 # Update the predator counter
			creatures.append(creature)	 # Get to remain on the target list
			
			if creature.stats["age"]%250 == 0 and randint(0,4)==0: # Creatures have a chance to periodically
				creatures.append( LiveCreature(1,[creature.x,creature.y],creature.var) )
				creatures[-1].vx, creatures[-1].vy = randint(-10,10), randint(-10,10)
				creatures[-1].stats["health"] = creature.stats["health"]
			
	# render
	draw() # Display the field
	#turncount += 1  # increase the number of turns by 1 # currently buggy - possibly a scope problem

	if predatorcensus > 0: ontimer(move, basetimer) # Recursively call move again, but only if we have predators still in play
	else: return 0 #turncount

setup(screensize[0]+screenedge, screensize[1]+screenedge) # Set up playfield
hideturtle()
up()
tracer(False)
result = move() # Play the game (recursively)
print("{}".format(result))
done()

